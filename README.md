# Example Python Projects

This project contains a collection of Python programs for you
to practice running programs.

## Instructions

1. Clone this repository to your _projects_ directory.
1. Follow the steps in your Solo Project to answer questions
   about each program
